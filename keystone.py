#!/usr/bin/env python3
import requests
import socket
import json
import sys

KEYSTONE_API = 'http://ks:35357/v3'
TOKEN = 'c9f8d3398d7a5813cb401952cbbcc0d8'

COMMANDS = ('list_roles', 'list_users', 'create_role', 'delete_role', 'list_groups', 'delete_group', 'role_to_group', 'list_roles_of_group', 'json_export')


def ksrequest(url, method='GET', data={}):
    H = {'X-Auth-Token': TOKEN}
    if method == 'PUT':
        H['Content-Type'] = 'application/json'
        res = requests.put(KEYSTONE_API + url, headers=H, data=data)
    elif method == 'POST':
        H['Content-Type'] = 'application/json'
        res = requests.post(KEYSTONE_API + url, headers=H, data=data)
    elif method == 'DELETE':
        res = requests.delete(KEYSTONE_API + url, headers=H, data=data)

    else:
        res = requests.get(KEYSTONE_API + url, headers=H, data=data)
    if res.status_code < 300:
        return res
    else:
        print('Issue during access to keystone API ({1}) with status code {0}. Returned error is \n{2}'.format(res.status_code, KEYSTONE_API + url, res.text))
        exit(1)


def get_ks_roles():
    """ returns a dict with role name as a key, id has a value """
    url='/roles'
    res = ksrequest(url)
    jsonres = json.loads(res.text)
    roles = jsonres['roles']
    roles_dict = {}
    for r in roles:
        roles_dict[r['name']] = r['id']

    return roles_dict


def get_ks_groups():
    url = '/groups'
    res = ksrequest(url)
    jsonres = json.loads(res.text)
    groups = jsonres['groups']
    groups_list = []
    for g in groups:
        groups_list.append(g['id'])

    return groups_list

def get_ks_users():
    url = '/users'
    res = ksrequest(url)
    jsonres = json.loads(res.text)
    users = jsonres['users']
    users_list = []
    for u in users:
        users_list.append(u['id'])

    return users_list


def create_role(arg):
    """ Create a role if doesn't exist """
    
    allroles = get_ks_roles()
    role = arg[0]
    if role in allroles:
        print("Role {} already exist.".format(role))
        exit(0)

    url='/roles'
    data = '{"role":{"name":"%s"}}' % role
    res = ksrequest(url, method='POST', data=data)
    jsonres = json.loads(res.text)
    newrole = jsonres['role']['id']
    print(newrole)
    return newrole


def list_roles():
    roles_dict = get_ks_roles()
    print('{} ID'.format('Role'.ljust(40))) 
    for r in roles_dict:
      print('{} {}'.format(r.ljust(40), roles_dict[r]))


def list_groups():
    groups = get_ks_groups()
    print('\n'.join(sorted(groups)))

def list_users():
    users = get_ks_users()
    print('\n'.join(sorted(users)))


def delete_group(arg):
    group = arg[0]
    all_groups = get_ks_groups()
    if group in all_groups:
        url = '/groups/{}'.format(group)
        ksrequest(url, method='DELETE')
        print('Deleted group {}'.format(group))
    else:
        print('Group {} doesn\'t exist'.format(group))



def delete_role(arg):
    role = arg[0]
    all_roles = get_ks_roles()
    if role in all_roles:
        url = '/roles/{}'.format(all_roles[role])
        ksrequest(url, method='DELETE')
        print('Deleted role {}'.format(role))
    else:
        print('Role {} doesn\'t exist'.format(role))
 

def role_to_group(arg):
    """ Create a role if it doesn't exist, and associate it to an AD group """
    role = arg[0]
    group = arg[1]
    all_roles = get_ks_roles()
    if role not in all_roles:
        print("Role {} doesn't exist, creating it...".format(role))
        roleid = create_role([role])
    else:
        roleid = all_roles[role]

    url = '/domains/default/groups/{}/roles/{}'.format(group, roleid)
    ksrequest(url, method='PUT')
    print('Associated AD group {} with Keystone role {}'.format(group, role))
    
   
def list_roles_of_group(arg):
    """ List of roles associated to an AD group """
    group = arg[0]
    allgroups = get_ks_groups()
    if group not in allgroups:
        print('AD Group doesn\' exist in Keystone'.format(group))
        exit(1)
    url = '/domains/default/groups/{}/roles'.format(group)
    roles = ksrequest(url).text
    jsonroles = json.loads(roles)
    grouproles = jsonroles['roles']
    if not grouproles:
        print('Group {} doesn\'t have any associated role'.format(group))
    else:
        for r in jsonroles['roles']:
            print(r['name'])


def json_export():
    """ Export list of dicts with each group and associated role"""
    export = []
    allgroups = get_ks_groups()
    for group in allgroups:
        url = '/domains/default/groups/{}/roles'.format(group)
        roles = ksrequest(url).text
        jsonroles = json.loads(roles)
        for i in jsonroles['roles']:
            assign = {}
            assign['name'] = group
            assign['role'] = i['name']
            export.append(assign)

    print(json.dumps(export, indent=4))
 

if __name__ == '__main__':
    socket.setdefaulttimeout(10)
    if len(sys.argv) < 2 or sys.argv[1] not in COMMANDS:
        print('Usage: {} <command> <args...>'.format(sys.argv[0]))
        print('  allowed commands are : {}'.format(', '.join(COMMANDS)))
        exit(1)

    if len(sys.argv) > 2:
        globals()[sys.argv[1]](sys.argv[2:])
    else:
        globals()[sys.argv[1]]()