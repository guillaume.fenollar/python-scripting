#!/usr/bin/env python
# encoding: utf8
"""
This script generate Powershell commands that can be run to add DNS records
in a Windows infrastructure. It uses Salt pillar as input.
"""
from salt import client
import subprocess
import os

ENV = [
"",
"-preprod",
"-int",
"-recsep",
"-recsma",
"-dev",
"-debug"
]

def get_salt_applications_info(app):
    # Extrait depuis Salt la config des applications (pillar .app)
    local = client.Caller()
    return local.cmd('pillar.get', app)

def extract_config():
    # Prend la liste des applications en entrée et génère un dict avec les app/ports à requêter
    salt_data = get_salt_applications_info('applications')
    apps = []
    for current_app in salt_data:
        for macro in current_app:
            for micro in current_app[macro].iteritems():
                if micro[0] != '_machine':
                    for config in micro[1].iteritems():
                        if config[0] == 'nginx':
                            apps.append(config[1]['server_name'])
    return apps

def check_dns(apps):
    faulty = []
    null = open(os.devnull, 'w')
    for a in apps:
        for e in ENV:
            cur = "{}{}.domain.local".format(a, e)
            ret = subprocess.call(["host", cur], stdout=null, stderr=null)
            if ret != 0:
                faulty.append(cur)
    return faulty


config = extract_config()
res =  check_dns(config)
if res:
  for record in res:
    for e in ENV:
      if e == '' :
        environment = 'prod'
        continue
      if e in record:
        environment = e.split('-')[1]
        break

    print("dnscmd 10.110.16.16 /RecordAdd domain2.local {} CNAME apl.{}.domain.local".format(record.split('.domain2.local')[0], environment))

else:
  print("All good!")