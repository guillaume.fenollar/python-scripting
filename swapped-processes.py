#!/usr/bin/env python
# coding: utf8
"""
Author: Guillaume Fenollar <guillaume@fenollar.fr>

Print all processes having memory pages swapped out, and the amount.
In case current machine is running docker containers, print the container’s name
Remember that tmpfs among other things can use swap space, thus leading to 
difference between usage and result of this script
"""


import os
import re
import subprocess

all_swapped_proc = {}
total_swap_size = 0

if os.getuid() != 0:
  print('This script needs to be run as root for obvious reasons. Exiting...')
  exit(1)

def get_containers():
  containers_map = {}
  res = subprocess.check_output('docker ps -q | xargs docker inspect \
    --format "{{.State.Pid}} {{.Name}}"', shell=True, universal_newlines=True)
  res = res.strip().split('\n')
  for r in res:
    containers_map[r.split()[0]] = r.split()[1].lstrip('/')
  
  return containers_map


regex = re.compile('[0-9]')
allproc = os.listdir('/proc/')

if os.path.exists('/run/docker.sock'):
  if os.listdir('/var/lib/docker/containers'):
    dockerized = True
    containers_map = get_containers()
else:
  dockerized = False
  containers_map = False
  

processes = [ p for p in allproc if regex.match(p) ]
for p in processes:
  try:
    with open('/proc/{0}/status'.format(p)) as f:
      if 'dockerized' in locals() and p in containers_map:
        proc_name = "Container {0} (PID {1})".format(containers_map[p], p)
      for l in f.readlines():
        if not 'proc_name' in locals() and l.startswith('Name:'):
          proc_name = "Process {0} (PID {1})".format(l.split()[1], p)
          continue
        if l.startswith('VmSwap:'):
          used_swap = int(l.split()[1])
          if used_swap > 0:
            total_swap_size = total_swap_size + used_swap
            all_swapped_proc[proc_name] = used_swap
          del used_swap
          del proc_name
  except IOError:
    # Volatile process dead since
    pass 


for p in all_swapped_proc:
  if all_swapped_proc[p]:
    print('{1} kB : {0}'.format(p, all_swapped_proc[p]))

print('{} kB : TOTAL '.format(total_swap_size))